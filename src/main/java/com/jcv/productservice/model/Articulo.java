package com.jcv.productservice.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Información del articulo")
@Entity
@Table(name = "articulo")
public class Articulo {

	@ApiModelProperty(notes = "Identificador del articulo",example="1")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ApiModelProperty(notes = "Nombre del articulo, debe tener maximo 20 caracteres", example="Smarwatch SST")
	@Size(max = 20, message = "El nombre debe tener maximo 20 caracteres")
	@Column(name = "nombre", nullable = false, length = 20)
	private String nombre;

	@ApiModelProperty(notes = "Descripcion del articulo, debe tener maximo 200 caracteres", example="El model SST cuenta con 15 funciones para el entrenamiento deportivo")
	@Size(max = 200, message = "La descripcion debe tener maximo 200 caracteres")
	@Column(name = "descripcion", nullable = false, length = 200)
	private String descripcion;

	@ApiModelProperty(notes = "Precio del producto", example="1988.00")
	@Column(name = "precio", nullable = false)
	private Float precio;

	@ApiModelProperty(notes = "Modelo del articulo, debe tener maximo 10 caracteres", example="SST")
	@Size(max = 10, message = "Nombres debe tener maximo 10 caracteres")
	@Column(name = "modelo", nullable = false, length = 10)
	private String modelo;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Float getPrecio() {
		return precio;
	}

	public void setPrecio(Float precio) {
		this.precio = precio;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

}
