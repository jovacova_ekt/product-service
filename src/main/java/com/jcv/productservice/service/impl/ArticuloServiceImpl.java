package com.jcv.productservice.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jcv.productservice.dao.IArticuloDAO;
import com.jcv.productservice.dto.ArticuloDTO;
import com.jcv.productservice.model.Articulo;
import com.jcv.productservice.service.IArticuloService;

@Service
public class ArticuloServiceImpl implements IArticuloService {

	@Autowired
	private IArticuloDAO articuloDAO;

	@Override
	public Articulo modificar(Articulo articulo) {
		// TODO Auto-generated method stub
		return this.articuloDAO.save(articulo);
	}

	@Override
	public Articulo listarId(int id) {
		// TODO Auto-generated method stub
		Optional<Articulo> opt = this.articuloDAO.findById(id);
		return opt.isPresent() ? opt.get() : new Articulo();
	}

	@Override
	public int modificacionRestringida(ArticuloDTO articuloDTO) {
		// TODO Auto-generated method stub
		return this.articuloDAO.modificacionRestringida(articuloDTO.getDescripcion(), articuloDTO.getNombre(), articuloDTO.getId());
	}

}
