package com.jcv.productservice.service;

public interface ICRUD<T> {

	T modificar(T t);

	T listarId(int id);
}