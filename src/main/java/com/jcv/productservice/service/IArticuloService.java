package com.jcv.productservice.service;

import com.jcv.productservice.dto.ArticuloDTO;
import com.jcv.productservice.model.Articulo;

public interface IArticuloService extends ICRUD<Articulo>{
	public int modificacionRestringida(ArticuloDTO articuloDTO);
}
