package com.jcv.productservice.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jcv.productservice.dto.ArticuloDTO;
import com.jcv.productservice.exception.ModeloNotFoundException;
import com.jcv.productservice.model.Articulo;
import com.jcv.productservice.service.IArticuloService;

@RestController
@RequestMapping("/articulo")
public class ArticuloRestController {

	@Autowired
	private IArticuloService articuloService;

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Articulo> listarId(@PathVariable("id") Integer id) {
		Articulo articulo = new Articulo();
		articulo = this.articuloService.listarId(id);
		if (articulo == null || articulo.getId() == null) {
			throw new ModeloNotFoundException("ID: " + id);
		}
		return new ResponseEntity<Articulo>(articulo, HttpStatus.OK);
	}

	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> actualizar(@Valid @RequestBody ArticuloDTO articuloDTO) {
		int response = 0;
		response = this.articuloService.modificacionRestringida(articuloDTO);

		if (response > 0) {
			return new ResponseEntity<Object>(HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(HttpStatus.BAD_REQUEST);
		}

	}

}
