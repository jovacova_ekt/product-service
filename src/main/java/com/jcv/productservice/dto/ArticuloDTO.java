package com.jcv.productservice.dto;

import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Actualización del articulo")
public class ArticuloDTO {

	@ApiModelProperty(notes = "Identificador del articulo",required=true, example="1")
	@Size(max = 10, message = "El Identificador debe tener maximo 10 caracteres")
	private Integer id;
	
	@ApiModelProperty(notes = "Descripcion del articulo, debe tener maximo 200 caracteres", required=true, example="El model SST cuenta con 15 funciones para el entrenamiento deportivo")
	@Size(max = 200, message = "La descripcion debe tener maximo 200 caracteres")
	private String descripcion;
	
	@ApiModelProperty(notes = "Nombre del articulo, debe tener maximo 20 caracteres", required=true, example="Smarwatch SST")
	@Size(max = 20, message = "El nombre debe tener maximo 20 caracteres")
	private String nombre;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
