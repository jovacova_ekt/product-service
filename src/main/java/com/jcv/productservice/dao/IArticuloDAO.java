package com.jcv.productservice.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.jcv.productservice.model.Articulo;

@Repository
public interface IArticuloDAO extends JpaRepository<Articulo, Integer> {

	// 
	@Modifying
	@Transactional
	@Query(value = "UPDATE articulo SET descripcion = :descripcionDTO, nombre= :nombreDTO WHERE id=:idDTO", nativeQuery = true)
	int modificacionRestringida(@Param("descripcionDTO") String descripcionDTO, @Param("nombreDTO") String nombreDTO, @Param("idDTO") Integer idDTO);
}
